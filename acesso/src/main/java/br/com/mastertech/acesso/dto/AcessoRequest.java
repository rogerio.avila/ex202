package br.com.mastertech.acesso.dto;

import br.com.mastertech.acesso.dto.builder.AcessoRequestBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class AcessoRequest {
    @NotNull(message = "O id do cliente não pode ser nulo.")
    @Min(value = 1, message = "O id do cliente deve ser um número positivo.")
    @JsonProperty("cliente_id")
    private Long clienteId;
    @NotNull(message = "O id da porta não pode ser nulo.")
    @Min(value = 1, message = "O id da porta deve ser um número positivo.")
    @JsonProperty("porta_id")
    private Long portaId;

    public AcessoRequest(Long clienteId, Long portaId) {
        this.clienteId = clienteId;
        this.portaId = portaId;
    }

    public static AcessoRequestBuilder builder() {
        return AcessoRequestBuilder.anAcessoRequest();
    }

    public Long getClienteId() {
        return clienteId;
    }

    public Long getPortaId() {
        return portaId;
    }

}
