package br.com.mastertech.acesso.exception;

import org.springframework.http.HttpStatus;

public class AcessoNaoEncontradoException extends AcessoSystemException {
    public AcessoNaoEncontradoException(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
