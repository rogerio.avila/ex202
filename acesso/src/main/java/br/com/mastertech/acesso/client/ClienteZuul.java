package br.com.mastertech.acesso.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ZUUL", configuration = ResilienciaConfig.class)
public interface ClienteZuul {

    @GetMapping("/cliente/{id}")
    Cliente getCliente(@PathVariable("id") Long id);

    @GetMapping("/porta/{id}")
    Porta getPorta(@PathVariable("id") Long id);
}