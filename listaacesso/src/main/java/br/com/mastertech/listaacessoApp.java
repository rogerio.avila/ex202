package br.com.mastertech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClients;

@SpringBootApplication
@RibbonClients
public class listaacessoApp {
    public static void main(String[] args) {
        SpringApplication.run(listaacessoApp.class, args);
    }
}
