package br.com.mastertech.acesso.exception;

import org.springframework.http.HttpStatus;

class AcessoSystemException extends Exception {
    private HttpStatus httpStatus;

    AcessoSystemException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
