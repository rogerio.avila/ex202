package com.mastertech.acesso.client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Porta {
    private Long id;
    @JsonProperty("andar")
    private String andar;
    @JsonProperty("sala")
    private String sala;

    public Porta(Long id, String andar, String sala) {
        this.id = id;
        this.andar = andar;
        this.sala = sala;
    }

    public Long getId() {
        return id;
    }

    public String getAndar() {
        return andar;
    }

    public String getSala() {
        return sala;
    }
}
