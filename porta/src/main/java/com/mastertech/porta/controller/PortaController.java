package com.mastertech.porta.controller;

import com.mastertech.porta.dto.PortaDto;
import com.mastertech.porta.entity.Porta;
import com.mastertech.porta.exception.PortaNotFoundException;
import com.mastertech.porta.mapper.DataMapper;
import com.mastertech.porta.service.PortaService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.net.URI;
import java.util.List;

@RestController
@Validated
@RequestMapping("/porta")
public class PortaController {

    private final PortaService portaService;

    public PortaController(PortaService portaService) {this.portaService=portaService;}

    @GetMapping
    public ResponseEntity getAllPortas(){

        List<PortaDto> portaDtoList = DataMapper.INSTANCE.portaToPortaDto(portaService.findAll());
        return ResponseEntity.ok(portaDtoList);
    }

    @GetMapping("/{id}")
    public ResponseEntity getPorta(@Valid
                                   @Min(value=1, message = "o id da porta dever ser maior que 0.")
                                   @PathVariable("id") Long portaId) throws PortaNotFoundException{
        PortaDto portaDto = DataMapper.INSTANCE.portaToPortaDto(portaService.findById(portaId));
        return ResponseEntity.ok(portaDto);
    }

    @PostMapping
    public ResponseEntity savePorta(@RequestBody @Valid PortaDto portaDto){
        Porta porta = DataMapper.INSTANCE.portaDtoToPorta(portaDto);
        PortaDto savedPortaDto = DataMapper.INSTANCE.portaToPortaDto(portaService.save(porta));
        return ResponseEntity.created(URI.create("")).body(savedPortaDto);
    }

}
