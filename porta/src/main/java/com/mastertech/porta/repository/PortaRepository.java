package com.mastertech.porta.repository;

import com.mastertech.porta.entity.Porta;
import org.springframework.data.repository.CrudRepository;

public interface PortaRepository extends CrudRepository<Porta, Long> {
}
