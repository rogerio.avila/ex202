package com.mastertech.acesso.client;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Cliente {

    private Long id;
    @JsonProperty("nome")
    private String nome;

    public Cliente(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
