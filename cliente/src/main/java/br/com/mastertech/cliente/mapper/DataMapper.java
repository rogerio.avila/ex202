package br.com.mastertech.cliente.mapper;

import br.com.mastertech.cliente.dto.ClienteDto;
import br.com.mastertech.cliente.entity.Cliente;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataMapper {
    DataMapper INSTANCE = Mappers.getMapper(DataMapper.class);

    Cliente clienteDtoToCliente(ClienteDto clienteDto);
    ClienteDto clienteToClienteDto(Cliente cliente);
    List<ClienteDto> clienteToClienteDto(List<Cliente> cliente);

//    Cartao cartaoDtoToCartao(CartaoDto cartaoDto);
//    @Mapping(target = "clienteId", source = "cliente.id")
//    CartaoDto cartaoToCartaoDto(Cartao cartao);
//    @Mapping(target = "clienteId", source = "cliente.id")
//    CartaoInvalidadoDto cartaoToCartaoSemEstadoDto(Cartao cartao);
//    List<CartaoDto> cartaoToCartaoDto(List<Cartao> cartao);

//    Pagto pagtoDtoToPagto(PagtoDto pagtoDto);
//    @Mapping(target = "cartaoId", source = "cartao.id")
//    PagtoDto pagtoToPagtoDto(Pagto pagto);
//    List<PagtoDto> pagtoToPagtoDto(List<Pagto> pagto);
}
