package com.mastertech.porta.exception;

import org.springframework.http.HttpStatus;

public class PortaException extends Exception{
    private HttpStatus httpStatus;

    PortaException(String message, HttpStatus httpStatus){
        super(message);
        this.httpStatus=httpStatus;
    }

    HttpStatus getHttpStatus() {return httpStatus;}
}
