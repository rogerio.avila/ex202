package com.mastertech.porta.dto;

import com.mastertech.porta.dto.builder.PortaDtoBuilder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PortaDto {

    private Long id;
    @NotNull(message = "a porta nao pode ser nulo.")
    @NotEmpty(message = "a porta nao pode ser vazio.")
    private String sala;

    @NotNull(message = "o andar nao pode ser nulo.")
    @NotEmpty(message = "o andar nao pode ser vazio.")
    private String andar;

    public PortaDto(Long id, String sala, String andar) {
        this.id = id;
        this.sala = sala;
        this.andar = andar;
    }


    public static PortaDtoBuilder builder(){return PortaDtoBuilder.aPortaDto();}

    public Long getId() {
        return id;
    }

    public String getSala() {
        return sala;
    }

    public String getAndar() {
        return andar;
    }
}
