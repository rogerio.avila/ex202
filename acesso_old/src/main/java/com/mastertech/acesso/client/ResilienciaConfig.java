package com.mastertech.acesso.client;

import feign.Feign;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ResilienciaConfig {

    @Bean
    public Feign.Builder builder(){

        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ServicoFallback(), RetryableException.class)
                .build();
        return Resilience4jFeign.builder(decorators);
    }
}
