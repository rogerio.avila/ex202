package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.entity.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarParaKafka(Acesso acesso){
        producer.send("spec4-rogerio-avila-1", acesso);
    }
}
