package br.com.mastertech.acesso.mapper;

import br.com.mastertech.acesso.dto.AcessoRequest;
import br.com.mastertech.acesso.entity.Acesso;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AcessoMapper {
    AcessoMapper INSTANCE = Mappers.getMapper(AcessoMapper.class);

    AcessoRequest accessToAccessRequest(Acesso acesso);

    Acesso accessRequestToAccess(AcessoRequest acessoRequest);
}
