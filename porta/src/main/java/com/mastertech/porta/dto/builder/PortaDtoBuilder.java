package com.mastertech.porta.dto.builder;

import com.mastertech.porta.dto.PortaDto;

public final class PortaDtoBuilder {
    private Long id;
    private String sala;
    private String andar;

    private PortaDtoBuilder() {
    }

    public static PortaDtoBuilder aPortaDto() {
        return new PortaDtoBuilder();
    }

    public PortaDtoBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public PortaDtoBuilder sala(String sala) {
        this.sala = sala;
        return this;
    }

    public PortaDtoBuilder andar(String andar) {
        this.andar = andar;
        return this;
    }

    public PortaDto build() {
        return new PortaDto(id, sala, andar);
    }
}
