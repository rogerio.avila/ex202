package com.mastertech.porta.service;


import com.mastertech.porta.entity.Porta;
import com.mastertech.porta.exception.PortaNotFoundException;
import com.mastertech.porta.repository.PortaRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PortaService {

    private final PortaRepository portaRepository;

    public PortaService(PortaRepository portaRepository){this.portaRepository=portaRepository;}

    public List<Porta> findAll(){
        Iterable<Porta> all = portaRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }

    public Porta findById(Long idPorta) throws PortaNotFoundException {
        Optional<Porta> porta = portaRepository.findById(idPorta);
        if(!porta.isPresent()){
            throw new PortaNotFoundException("Porta nao encontrada !!");
        }
        return porta.get();
    }

    public Porta save(Porta porta) {return portaRepository.save(porta);}
}
