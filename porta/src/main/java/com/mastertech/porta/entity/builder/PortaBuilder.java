package com.mastertech.porta.entity.builder;

import com.mastertech.porta.entity.Porta;

public final class PortaBuilder {
    private Long id;
    private String sala;
    private String andar;

    private PortaBuilder() {
    }

    public static PortaBuilder aPorta() {
        return new PortaBuilder();
    }

    public PortaBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public PortaBuilder sala(String sala) {
        this.sala = sala;
        return this;
    }

    public PortaBuilder andar(String andar) {
        this.andar = andar;
        return this;
    }

    public Porta build() {
        return new Porta(id, sala, andar);
    }
}
