package br.com.mastertech.acesso.controller;

import br.com.mastertech.acesso.dto.AcessoRequest;
import br.com.mastertech.acesso.entity.Acesso;
import br.com.mastertech.acesso.exception.AcessoNaoEncontradoException;
import br.com.mastertech.acesso.mapper.AcessoMapper;
import br.com.mastertech.acesso.service.AcessoProducer;
import br.com.mastertech.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.net.URI;

@RestController
@Validated
@RequestMapping("/acesso")
public class AcessoController {

    private final AcessoService acessoService;

    @Autowired
    private AcessoProducer acessoProducer;

    public AcessoController(AcessoService acessoService) {
        this.acessoService = acessoService;
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    public ResponseEntity getAccess(
            @Valid
            @NotNull(message = "O id do cliente não pode ser nulo.")
            @Min(value = 1, message = "O id do cliente deve ser um número positivo.")
            @PathVariable("cliente_id") Long clienteId,
            @Valid
            @NotNull(message = "O id da porta não pode ser nulo.")
            @Min(value = 1, message = "O id da porta deve ser um número positivo.")
            @PathVariable("porta_id") Long portaId) throws AcessoNaoEncontradoException {
        Acesso acesso = acessoService.findByClienteIdAndPortaId(clienteId, portaId);
        acessoProducer.enviarParaKafka(acesso);
        return ResponseEntity.ok(AcessoMapper.INSTANCE.accessToAccessRequest(acesso));
    }

    @PostMapping
    public ResponseEntity createAccess(@RequestBody @Valid AcessoRequest acessoRequest) {
        Acesso savedAcesso = acessoService.saveAccess(AcessoMapper.INSTANCE.accessRequestToAccess(acessoRequest));
        acessoProducer.enviarParaKafka(savedAcesso);
        return ResponseEntity.created(URI.create("")).body(AcessoMapper.INSTANCE.accessToAccessRequest(savedAcesso));
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    public ResponseEntity deleteAccess(@Valid
                                       @NotNull(message = "O id do cliente não pode ser nulo.")
                                       @Min(value = 1, message = "O id do cliente deve ser um número positivo.")
                                       @PathVariable("cliente_id") Long clienteId,
                                       @Valid
                                       @NotNull(message = "O id da porta não pode ser nulo.")
                                       @Min(value = 1, message = "O id da porta deve ser um número positivo.")
                                       @PathVariable("porta_id") Long portaId) {
        acessoService.deleteByClienteIdAndPortaId(clienteId, portaId);
        return ResponseEntity.noContent().build();
    }

//    @PostMapping
//    public void enviaAcesso(@RequestBody Acesso acesso){
//        acessoProducer.enviarParaKafka(acesso);
//    }
}
