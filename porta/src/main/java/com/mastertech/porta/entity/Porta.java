package com.mastertech.porta.entity;

import com.mastertech.porta.entity.builder.PortaBuilder;

import javax.persistence.*;

@Entity
@Table(name="porta")
public class Porta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String sala;
    @Column
    private String andar;

    public Porta() {
    }

    public Porta(Long id, String sala, String andar) {

        this.id=id;
        this.sala = sala;
        this.andar = andar;
    }

    public static PortaBuilder builder() { return PortaBuilder.aPorta();}

    public Long getId() {
        return id;
    }

    public String getSala() {
        return sala;
    }

    public String getAndar() {
        return andar;
    }
}
