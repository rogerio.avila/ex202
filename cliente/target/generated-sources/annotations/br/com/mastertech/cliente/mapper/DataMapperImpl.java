package br.com.mastertech.cliente.mapper;

import br.com.mastertech.cliente.dto.ClienteDto;
import br.com.mastertech.cliente.dto.builder.ClienteDtoBuilder;
import br.com.mastertech.cliente.entity.Cliente;
import br.com.mastertech.cliente.entity.builder.ClienteBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-18T15:03:48-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (JetBrains s.r.o)"
)
@Component
public class DataMapperImpl implements DataMapper {

    @Override
    public Cliente clienteDtoToCliente(ClienteDto clienteDto) {
        if ( clienteDto == null ) {
            return null;
        }

        ClienteBuilder cliente = Cliente.builder();

        cliente.nome( clienteDto.getNome() );

        return cliente.build();
    }

    @Override
    public ClienteDto clienteToClienteDto(Cliente cliente) {
        if ( cliente == null ) {
            return null;
        }

        ClienteDtoBuilder clienteDto = ClienteDto.builder();

        clienteDto.id( cliente.getId() );
        clienteDto.nome( cliente.getNome() );

        return clienteDto.build();
    }

    @Override
    public List<ClienteDto> clienteToClienteDto(List<Cliente> cliente) {
        if ( cliente == null ) {
            return null;
        }

        List<ClienteDto> list = new ArrayList<ClienteDto>( cliente.size() );
        for ( Cliente cliente1 : cliente ) {
            list.add( clienteToClienteDto( cliente1 ) );
        }

        return list;
    }
}
