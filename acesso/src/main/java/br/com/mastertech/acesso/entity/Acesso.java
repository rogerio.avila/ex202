package br.com.mastertech.acesso.entity;

import br.com.mastertech.acesso.entity.builder.AcessoBuilder;

import javax.persistence.*;

@Entity
@Table(name = "acesso")
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "cliente_id")
    private Long clienteId;
    @Column(name = "porta_id")
    private Long portaId;

    public Acesso() {
    }

    public Acesso(Long clienteId, Long portaId) {
        this.clienteId = clienteId;
        this.portaId = portaId;
    }

    public static AcessoBuilder builder() {
        return AcessoBuilder.anAcesso();
    }

    public Long getId() {
        return id;
    }

    public Long getClienteId() {
        return clienteId;
    }

    public Long getPortaId() {
        return portaId;
    }

}
