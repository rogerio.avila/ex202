package com.mastertech.porta.mapper;

import com.mastertech.porta.dto.PortaDto;
import com.mastertech.porta.entity.Porta;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DataMapper {
    DataMapper INSTANCE = Mappers.getMapper(DataMapper.class);

    Porta portaDtoToPorta(PortaDto portaDto);
    PortaDto portaToPortaDto(Porta porta);
    List<PortaDto> portaToPortaDto(List<Porta> porta);

}
