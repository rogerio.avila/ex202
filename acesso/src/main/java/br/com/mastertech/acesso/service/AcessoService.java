package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.client.ClienteZuul;
import br.com.mastertech.acesso.entity.Acesso;
import br.com.mastertech.acesso.exception.AcessoNaoEncontradoException;
import br.com.mastertech.acesso.repository.AcessoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AcessoService {

    private final AcessoRepository acessoRepository;
    private final ClienteZuul clienteZuul;

    public AcessoService(AcessoRepository acessoRepository, ClienteZuul clienteZuul) {
        this.acessoRepository = acessoRepository;
        this.clienteZuul = clienteZuul;
    }

    public Acesso saveAccess(Acesso acesso) {
        return acessoRepository.save(acesso);
    }

    public Acesso findByClienteIdAndPortaId(Long clienteId, Long portaId) throws AcessoNaoEncontradoException {
        clienteZuul.getCliente(clienteId);
        clienteZuul.getPorta(portaId);
        return acessoRepository.findByClienteIdAndPortaId(clienteId, portaId)
                .orElseThrow(() -> new AcessoNaoEncontradoException("O acesso informado não foi encontrado."));
    }

    public void deleteByClienteIdAndPortaId(Long clienteId, Long portaId) {
        clienteZuul.getCliente(clienteId);
        clienteZuul.getPorta(portaId);
        acessoRepository.deleteByClienteIdAndPortaId(clienteId, portaId);
    }
}
