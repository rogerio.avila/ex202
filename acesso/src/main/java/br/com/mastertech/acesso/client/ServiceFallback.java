package br.com.mastertech.acesso.client;

import br.com.mastertech.acesso.exception.ClienteServicoForaException;
import br.com.mastertech.acesso.exception.PortaServicoForaException;

public class ServiceFallback implements ClienteZuul {
    @Override
    public Cliente getCliente(Long id) {
        throw new ClienteServicoForaException();
    }

    @Override
    public Porta getPorta(Long id) {
        throw new PortaServicoForaException();
    }
}
