package com.mastertech.acesso.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.SERVICE_UNAVAILABLE, reason = "API de porta esta fora do ar, tente novamente.")
public class PortaServicoForaException extends RuntimeException{
}
