package br.com.mastertech.acesso.entity.builder;

import br.com.mastertech.acesso.entity.Acesso;

public final class AcessoBuilder {
    private Long clienteId;
    private Long portaId;

    private AcessoBuilder() {
    }

    public static AcessoBuilder anAcesso() {
        return new AcessoBuilder();
    }

    public AcessoBuilder clienteId(Long clienteId) {
        this.clienteId = clienteId;
        return this;
    }

    public AcessoBuilder portaId(Long portaId) {
        this.portaId = portaId;
        return this;
    }

    public Acesso build() {
        return new Acesso(clienteId, portaId);
    }
}
