package br.com.mastertech.acesso.entity;

public class Acesso {

        private Long id;
        private Long clienteId;
        private Long portaId;

        public Acesso() {
        }

        public Long getId() {
            return id;
        }

    public Acesso(Long id, Long clienteId, Long portaId) {
        this.id = id;
        this.clienteId = clienteId;
        this.portaId = portaId;
    }

    public Long getClienteId() {
            return clienteId;
        }

        public Long getPortaId() {
            return portaId;
        }
    }
