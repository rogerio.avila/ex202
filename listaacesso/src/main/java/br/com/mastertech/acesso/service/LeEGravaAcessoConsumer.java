package br.com.mastertech.acesso.service;

import br.com.mastertech.acesso.entity.Acesso;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class LeEGravaAcessoConsumer {
    @KafkaListener(topics = "spec4-rogerio-avila-2", groupId = "acesso")
    public void receiveAccessInfo(@Payload Acesso acesso) throws IOException {
        gravaNoArquitoTXT(acesso);
    }

    private void gravaNoArquitoTXT(Acesso acesso) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd_MM_yyyy");
        String dateReport = LocalDate.now().format(formatter);
        File txtFile = new File("acesso_" + dateReport + ".txt");
        FileWriter out = new FileWriter(txtFile, true);
        if (txtFile.length() > 0) {
            try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT)) {
                addRecord(acesso, printer);
            }
        } else {
            String[] headers = {"Cliente", "Porta", "Possui Acesso"};
            try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(headers))) {
                addRecord(acesso, printer);
            }
        }
    }

    private void addRecord(Acesso acesso, CSVPrinter printer) throws IOException {
        printer.printRecord(acesso.getClienteId(), acesso.getPortaId(), "ok");
        System.out.println("Peguei mais um cliente.");
    }
}