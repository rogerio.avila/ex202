package br.com.mastertech.acesso.mapper;

import br.com.mastertech.acesso.dto.AcessoRequest;
import br.com.mastertech.acesso.dto.builder.AcessoRequestBuilder;
import br.com.mastertech.acesso.entity.Acesso;
import br.com.mastertech.acesso.entity.builder.AcessoBuilder;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-08-19T16:33:37-0300",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.6 (JetBrains s.r.o)"
)
@Component
public class AcessoMapperImpl implements AcessoMapper {

    @Override
    public AcessoRequest accessToAccessRequest(Acesso acesso) {
        if ( acesso == null ) {
            return null;
        }

        AcessoRequestBuilder acessoRequest = AcessoRequest.builder();

        acessoRequest.clienteId( acesso.getClienteId() );
        acessoRequest.portaId( acesso.getPortaId() );

        return acessoRequest.build();
    }

    @Override
    public Acesso accessRequestToAccess(AcessoRequest acessoRequest) {
        if ( acessoRequest == null ) {
            return null;
        }

        AcessoBuilder acesso = Acesso.builder();

        acesso.clienteId( acessoRequest.getClienteId() );
        acesso.portaId( acessoRequest.getPortaId() );

        return acesso.build();
    }
}
