package com.mastertech.acesso.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="Zuul", configuration = ResilienciaConfig.class)
public interface ClienteZuul {

    @GetMapping("/cliente/{id}")
    Cliente getCustomer(@PathVariable("id") Long id);

    @GetMapping("/porta/{id}")
    Porta getDoor(@PathVariable("id") Long id);
}
