package br.com.mastertech.acesso.model.builder;

import br.com.mastertech.acesso.model.Erro;

import java.time.LocalDateTime;
import java.util.List;

public final class ErroBuilder {
    private List<String> messages;
    private LocalDateTime timestamp;

    private ErroBuilder() {
    }

    public static ErroBuilder anErro() {
        return new ErroBuilder();
    }

    public ErroBuilder messages(List<String> messages) {
        this.messages = messages;
        return this;
    }

    public ErroBuilder timestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public Erro build() {
        return new Erro(messages, timestamp);
    }
}
