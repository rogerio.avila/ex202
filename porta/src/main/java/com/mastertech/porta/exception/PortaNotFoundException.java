package com.mastertech.porta.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Porta não encontrada !!")
public class PortaNotFoundException  extends PortaException{

    public PortaNotFoundException(String message){ super(message, HttpStatus.NOT_FOUND);}
}
