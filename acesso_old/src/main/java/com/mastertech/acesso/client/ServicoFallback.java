package com.mastertech.acesso.client;

public class ServicoFallback implements ClienteZuul {

    @Override
    public Cliente getCliente(Long id){
        throw new ClienteServicoForaException();
    }

    @Override
    public Porta getPorta(Long id){
        throw new PortaServicoForaException();
    }
}
