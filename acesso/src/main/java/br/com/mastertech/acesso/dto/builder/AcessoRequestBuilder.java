package br.com.mastertech.acesso.dto.builder;

import br.com.mastertech.acesso.dto.AcessoRequest;

public final class AcessoRequestBuilder {
    private Long clienteId;
    private Long portaId;

    private AcessoRequestBuilder() {
    }

    public static AcessoRequestBuilder anAcessoRequest() {
        return new AcessoRequestBuilder();
    }

    public AcessoRequestBuilder clienteId(Long clienteId) {
        this.clienteId = clienteId;
        return this;
    }

    public AcessoRequestBuilder portaId(Long portaId) {
        this.portaId = portaId;
        return this;
    }

    public AcessoRequest build() {
        return new AcessoRequest(clienteId, portaId);
    }
}
