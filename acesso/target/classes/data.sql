DROP TABLE IF EXISTS acesso CASCADE;

CREATE TABLE acesso
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    cliente_Id   INT,
    porta_Id     INT
);