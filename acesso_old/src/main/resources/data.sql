DROP TABLE IF EXISTS acesso CASCADE;

CREATE TABLE acesso
(
    id          INT AUTO_INCREMENT PRIMARY KEY,
    clienteId INT NOT NULL,
    portaId     INT NOT NULL
);