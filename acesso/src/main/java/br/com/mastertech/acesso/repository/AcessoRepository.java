package br.com.mastertech.acesso.repository;

import br.com.mastertech.acesso.entity.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
    Optional<Acesso> findByClienteIdAndPortaId(Long clienteId, Long portaId);

    void deleteByClienteIdAndPortaId(Long clienteId, Long portaId);
}
